# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/04/10 13:37:24 by lorenuar          #+#    #+#              #
#    Updated: 2021/06/25 17:58:09 by lorenuar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# ================================ VARIABLES ================================= #

MAKE = make --no-print-directory

SHELL = /bin/bash

FLD_LIBUTILS = lib_utils
BIN_LIBUTILS = ./$(FLD_LIBUTILS)/libutils.a

FLD_LIBCOMMON = lib_common
BIN_LIBCOMMON = ./$(FLD_LIBCOMMON)/libcommon.a

FLD_CHECKER = prg_checker
BIN_CHECKER = ./$(FLD_CHECKER)/checker
PRG_CHECKER = $(notdir $(BIN_CHECKER))

FLD_PUSH_SWAP = prg_push_swap
BIN_PUSH_SWAP = ./$(FLD_PUSH_SWAP)/push_swap
PRG_PUSH_SWAP = $(notdir $(BIN_PUSH_SWAP))

FLD_PRG = $(FLD_CHECKER) $(FLD_PUSH_SWAP)
BIN_PRG = $(BIN_CHECKER) $(BIN_PUSH_SWAP)

ifndef L
FLD_LIB = $(FLD_LIBUTILS)
FLD_LIB += $(FLD_LIBCOMMON)
endif
BIN_LIB = $(BIN_LIBUTILS) $(BIN_LIBCOMMON)

FLD_ALL := $(FLD_LIB) $(FLD_PRG)
BIN_ALL := $(BIN_LIB) $(BIN_PRG)

ifndef NUM
	NUM = 3
endif

# Colors
GR	= \033[32;1m
RE	= \033[31;1m
YE	= \033[33;1m
CY	= \033[36;1m
RC	= \033[0m

# ================================== RULES =================================== #

.PHONY: all $(FLD_ALL) clean fclean re run rm_execs backup

all : $(FLD_ALL)
	@rm -f $(PRG_PUSH_SWAP) $(PRG_CHECKER)
	@printf "$(YE)=-=-=-=-=-=-=-= MAKE === COPY $(BIN_PUSH_SWAP) to ./$(PRG_PUSH_SWAP)$(RC)\n"
	@cp $(BIN_PUSH_SWAP) .
	@printf "$(YE)=-=-=-=-=-=-=-= MAKE === COPY $(BIN_CHECKER) to ./$(PRG_CHECKER)$(RC)\n"
	@cp $(BIN_CHECKER) .
	@printf "$(GR)=-=-=-=-=-=-=-= MAKE === MADE EVERYTHING SUCCESSFULLY ===$(RC)\n"

$(FLD_ALL) :
	@printf "$(GR)=-=-=-=-=-=-=-= MAKE SUBS === $@ [$(MAKECMDGOALS)]$(RC)\n"
	@$(MAKE) -C $@ $(MAKECMDGOALS)

clean : $(FLD_ALL)

fclean : $(FLD_ALL)
	@printf "$(RE)=-=-=-=-=-=-=-= MAKE === --- Removing ./$(PRG_CHECKER) ./$(PRG_PUSH_SWAP)$(RC)\n"
	@rm -f $(notdir $(PRG_CHECKER)) $(notdir $(PRG_PUSH_SWAP))

# Special rule to force to remake everything
re : MAKECMDGOALS = re
re : fclean all

# This runs the program
run : MAKECMDGOALS = all
run : all
	@printf "$(CY)>>> Running test$(RC) "
	./test.sh $(NUM)
