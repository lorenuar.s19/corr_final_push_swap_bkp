/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libutils.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/06 15:05:51 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/24 18:47:53 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBUTILS_H
# define LIBUTILS_H

# include "lib_chr.h"
# include "lib_io.h"
# include "lib_list.h"
# include "lib_nbr.h"
# include "lib_stack.h"
# include "lib_gnst.h"
# include "lib_str.h"
# include "lib_math.h"
# include "ft_printf.h"

#endif
