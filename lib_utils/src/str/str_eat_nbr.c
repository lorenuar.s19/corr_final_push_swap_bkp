/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_eat_nbr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/08 18:00:31 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/30 17:21:47 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "lib_str.h"
#include "lib_nbr.h"
#include "lib_chr.h"

static int	ret_eat_nbr(ssize_t *num, char sign)
{
	if ((*num) > LONG_MAX)
	{
		if (sign == 1)
			(*num) = -1;
		else if (sign == -1)
			(*num) = 0;
	}
	else if (sign == -1)
		(*num) = - (*num);
	return (0);
}

int	str_eat_nbr(char **f, ssize_t *num)
{
	char		sign;

	if (!f || !*f || !num)
		return (1);
	(*num) = 0;
	sign = 1;
	while ((f && *f && is_wsp(**f)))
		(*f)++;
	if (f && *f && **f == '-')
	{
		sign = -1;
		(*f)++;
	}
	if (f && *f && !is_digit(**f))
		return (1);
	while (f && *f && is_digit(**f))
	{
		(*num) = ((*num) * BASE_DEFAULT) + (**f - '0');
		(*f)++;
	}
	return (ret_eat_nbr(num, sign));
}
