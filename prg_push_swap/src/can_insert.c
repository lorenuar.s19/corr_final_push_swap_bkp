/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   can_insert.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/24 18:40:26 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	sub_select_nearest(t_sdat *sd, t_bmdat *bd)
{
	t_range	tmp;

	tmp = bd->best_pos;
	if (tmp.min >= 3 && tmp.min > (sd->siz.a / 2))
	{
		tmp.min = tmp.min - sd->siz.a;
	}
	if (tmp.max >= 3 && (tmp.max > sd->siz.a / 2))
	{
		tmp.max = tmp.max - sd->siz.a;
	}
	if (tmp.max < tmp.min)
	{
		bd->pos.a = bd->best_pos.max + 1;
	}
	else if (tmp.min < tmp.max)
	{
		bd->pos.a = bd->best_pos.min;
	}
}

static void	sub_set_pos_a(t_sdat *sd, t_bmdat *bd, t_fcp *fcp)
{
	if (fcp->cur == 0 && bd->best_pos.min >= 0)
	{
		bd->pos.a = bd->best_pos.min;
	}
	else if (fcp->cur == fcp->a.max && bd->best_pos.min >= 0)
	{
		bd->pos.a = bd->best_pos.min;
	}
	else if (bd->best_pos.max >= 0 && bd->best_pos.min < 0)
	{
		bd->pos.a = bd->best_pos.max + 1;
	}
	else if (bd->best_pos.min >= 0 && bd->best_pos.max < 0)
	{
		bd->pos.a = bd->best_pos.min;
	}
	else if (bd->best_pos.max >= 0 && bd->best_pos.min >= 0)
	{
		sub_select_nearest(sd, bd);
	}
}

static void	get_min_max_ab(t_sdat *sd, t_fcp *fcp)
{
	int	i;
	int	pos;

	i = 0;
	while (i >= 0 && i < sd->siz.a)
	{
		pos = sd->postab[i].pos;
		if (pos < fcp->a.min)
			fcp->a.min = pos;
		if (pos > fcp->a.max)
			fcp->a.max = pos;
		i++;
	}
	while (i >= sd->siz.a && i < sd->inp_siz)
	{
		pos = sd->postab[i].pos;
		if (pos < fcp->b.min)
			fcp->b.min = pos;
		if (pos > fcp->b.max)
			fcp->b.max = pos;
		i++;
	}
}

int	can_insert(t_sdat *sd, t_bmdat *bd)
{
	t_fcp	fcp;

	fcp = (t_fcp){0, 0, 0,
		(t_range){sd->inp_siz, 0}, (t_range){sd->inp_siz, 0},
		(t_range){0, sd->inp_siz - 1}};
	fcp.cur = sd->postab[bd->pos.b].pos;
	get_min_max_ab(sd, &fcp);
	find_best_place(sd, bd, &fcp);
	sub_set_pos_a(sd, bd, &fcp);
	bd->moves.a = bd->pos.a;
	if (sd->siz.a > 3 && bd->pos.a >= sd->siz.a / 2 && bd->pos.a < sd->siz.a)
	{
		bd->moves.a = bd->pos.a - sd->siz.a;
	}
	bd->moves.b = bd->pos.b - sd->siz.a;
	if (sd->siz.b > 3 && bd->pos.b - sd->siz.a >= sd->siz.b / 2
		&& bd->pos.b < sd->inp_siz)
	{
		bd->moves.b = (bd->pos.b - sd->siz.a) - sd->siz.b;
	}
	sd->push_type = 1;
	return (0);
}
