/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mark_to_split.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/30 14:16:05 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include <stdlib.h>
#include "push_swap.h"

/*
** Find longest increasing subsequence (LIS)
*/

static int	select_best(t_gnst	*super, t_stack **result)
{
	t_stack	*tmp;
	t_best	best;
	int		y;

	best = (t_best){NULL, 0, 0};
	y = 0;
	while (y < super->siz)
	{
		tmp = (t_stack *)super->dat[y];
		if (tmp->siz > best.len)
		{
			best = (t_best){tmp, tmp->siz, y};
		}
		y++;
	}
	*result = best.best;
	return (0);
}

static int	sub_find_lis(t_sdat *sd, t_gnst *super, int i)
{
	t_stack	*tmp_stack;

	tmp_stack = malloc(sizeof(t_stack));
	*tmp_stack = (t_stack){NULL, 0, 0};
	if (!tmp_stack)
		return (error_put(1, "sub_find_lis : tmp_stack NULL"));
	if (gnst_push_back(super, tmp_stack))
		return (error_put(1, "sub_find_lis : lst_push_back()"));
	if (build_inc_sequence(sd, super->dat[super->siz - 1], i))
		return (error_put(1, "sub_find_lis : build_inc_sequence()"));
	return (0);
}

static int	find_lis(t_sdat *sd, t_gnst *super, t_stack **result)
{
	int		start_visited;
	int		i;

	start_visited = 0;
	i = 0;
	while (i >= 0 && i < sd->inp_siz)
	{
		if (i == 0)
			start_visited++;
		if (start_visited > 1)
			break ;
		if (sub_find_lis(sd, super, i))
			return (error_put(1, "find_lis : sub_find_lis()"));
		i++;
		if (i >= sd->inp_siz)
			i = 0;
	}
	if (select_best(super, result))
		return (error_put(1, "find_lis : select_best()"));
	return (0);
}

void	free_all_mts(t_gnst *super)
{
	t_stack	*tmp;
	int		i;

	i = 0;
	while (i < super->siz)
	{
		tmp = (t_stack *)super->dat[i];
		stack_free(tmp);
		free(tmp);
		i++;
	}
	gnst_free(super);
}

int	mark_to_split(t_sdat *sd, t_stack *lis)
{
	t_stack	*result;
	t_gnst	super;

	super = (t_gnst){NULL, 0, 0};
	pos_table_reset_split(sd, 0);
	if (find_lis(sd, &super, &result))
	{
		free_all_mts(&super);
		return (error_put(1, "mark_to_split : find_lis"));
	}
	if (copy_stack(result, lis))
	{
		free_all_mts(&super);
		return (error_put(1, "mark_to_split : set_sorted()"));
	}
	free_all_mts(&super);
	return (0);
}
