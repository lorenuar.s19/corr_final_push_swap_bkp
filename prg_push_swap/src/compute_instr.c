/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute_instr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/12 20:56:04 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/30 14:13:38 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	compute_instr(t_sdat *sd)
{
	t_stack	lis;

	lis = (t_stack){NULL, 0, 0};
	if (sd->inp_siz > 5)
	{
		if (mark_to_split(sd, &lis))
		{
			stack_free(&lis);
			return (error_put(1, E_CI0));
		}
	}
	if (split_to_b(sd, &lis))
	{
		stack_free(&lis);
		return (error_put(1, E_CI1));
	}
	stack_free(&lis);
	if (merge_back(sd))
		return (error_put(1, E_CI2));
	check_pre_sorted(sd);
	if (put_zero_on_top(sd))
		return (1);
	if (stack_check_sorted_ascending(sd->sta))
		return (error_put(1, "compute_instr : stack is not sorted"));
	return (0);
}
