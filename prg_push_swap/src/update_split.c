/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_split.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/25 16:10:23 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/25 16:15:52 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	update_split(t_sdat *sd, t_stack *lis)
{
	int	ind;
	int	i;

	i = 0;
	pos_table_reset_split(sd, 0);
	while (i < lis->siz)
	{
		ind = get_index_pos(sd, lis->dat[i]);
		sd->postab[ind].sorted = 1;
		i++;
	}
	return (0);
}
