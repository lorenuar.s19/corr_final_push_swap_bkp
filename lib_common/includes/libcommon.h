/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libcommon.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 15:50:11 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/27 18:17:47 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBCOMMON_H
# define LIBCOMMON_H

# include "libutils.h"

# define COL1 2
# define COL2 3
# define COL3 4

# define DELAY 2

enum	e_instructions
{
	I_NVALID = -1,
	I_SA = 0,
	I_SB,
	I_SS,
	I_PA,
	I_PB,
	I_RA,
	I_RB,
	I_RR,
	I_RRA,
	I_RRB,
	I_RRR,
};

# define N_INSTR 11

typedef struct s_range
{
	int		min;
	int		max;
}				t_range;

typedef struct s_pair
{
	int		a;
	int		b;
}	t_pair;

typedef struct s_pos_table
{
	int			pos;
	int			number;
	int			sorted;
}				t_ptab;

typedef struct s_sorter_data
{
	t_stack		*sta;
	t_stack		*stb;

	t_range		*range_table;
	t_range		range;
	int			range_i_max;

	t_ptab		*postab;
	t_pair		siz;
	int			inp_siz;

	int			push_type;
	int			best_push_type;

	int			verbose;

}				t_sdat;

int		stacks_parse_args(int argc, char *argv[], t_stack *a, int verbose);

void	stacks_free_all(t_stack *a, t_stack *b);

int		stack_check_valid(t_stack *sta);

int		stack_check_sorted_ascending(t_stack *sta);
int		stack_check_sorted_descending(t_stack *sta);

int		read_instrs(int fd);
int		exec_instr(t_stack *sta, t_stack *stb, int instr, t_sdat *sd);

int		i_swap_a(t_stack *sta, t_stack *stb);
int		i_swap_b(t_stack *sta, t_stack *stb);
int		i_swap_both(t_stack *sta, t_stack *stb);

int		i_push_a(t_stack *sta, t_stack *stb);
int		i_push_b(t_stack *sta, t_stack *stb);

int		i_rot_a(t_stack *sta, t_stack *stb);
int		i_rot_b(t_stack *sta, t_stack *stb);
int		i_rot_both(t_stack *sta, t_stack *stb);

int		i_revrot_a(t_stack *sta, t_stack *stb);
int		i_revrot_b(t_stack *sta, t_stack *stb);
int		i_revrot_both(t_stack *sta, t_stack *stb);

int		setup_pos_table(t_sdat *sd);
int		populate_pos_table(t_sdat *sd);
int		update_pos_table(t_sdat *sd);

int		parse_opts(char argc, char *argv[]);

typedef struct s_print_data
{
	t_stack		*sta;
	t_stack		*stb;
	ssize_t		ia;
	ssize_t		ib;
	ssize_t		siz_a;
	ssize_t		siz_b;
	int			pad_b;
	int			pad_n;
}				t_pdata;

# define SHORT_MAX_NODES 5

void	print_stacks(t_stack *a, t_stack *b, int instr, int verbose);
void	print_stacks_draw(t_stack *a, t_stack *b, int instr, t_sdat *sd);
void	sub_print_instr(int instr);

/*
** Errors
*/

# define E_SPA0 "stacks_parse_args : error from stack_from_str()"
# define E_SPA1 "stacks_parse_args : error from stack_from_args()"
# define E_PI0 "parse_instr : Instruction not found"
# define E_RI0 "read_instr : Invalid string content"
# define E_SCS0 "stack_check_sorted : input is NULL"
# define E_SCV0 "stack_check_valid : input is NULL"
# define E_SCV1 "stack_check_valid : node is out of integer range"
# define E_SPS0 "stacks_parse_args : stack is empty"
# define E_EI0 "exec_instr : instruction out of range"

# define E_SIT0  "setup_pos_table : create_pos_table()"
# define E_SIT1  "setup_pos_table : get_min_max()"
# define E_SIT2  "setup_pos_table : populate_pos_table()"
# define E_UPT0  "update_pos_tables : populate_pos_table()"
# define E_FNFS0 "fill_numbers_from_stack : NULL input"
# define E_PCV0  "postab_check_valid : found unused index"
# define E_PPT0  "setup_pos_table : fill_numbers_from_stack()"

#endif
