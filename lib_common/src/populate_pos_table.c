/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   populate_pos_table.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/10 00:15:54 by lorenuar          #+#    #+#             */
/*   Updated: 2021/07/02 10:38:15 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "libcommon.h"

static int	fill_numbers_from_stack(t_sdat *sd)
{
	ssize_t	i;
	ssize_t	j;

	if (!sd)
		return (error_put(1, E_FNFS0));
	sd->siz.a = stack_get_size(sd->sta);
	sd->siz.b = stack_get_size(sd->stb);
	i = 0;
	while (sd->sta && sd->sta->dat && i < sd->sta->siz && i < sd->inp_siz)
	{
		sd->postab[i].number = sd->sta->dat[i];
		i++;
	}
	j = 0;
	while (i < sd->inp_siz && sd->stb && sd->stb->dat
		&& j >= 0 && j < sd->stb->siz)
	{
		sd->postab[i].number = sd->stb->dat[j];
		i++;
		j++;
	}
	return (0);
}

static int	postab_check_valid(t_sdat *sd)
{
	ssize_t	i;

	i = 0;
	while (i < sd->inp_siz)
	{
		if (sd->postab[i].pos == -1)
		{
			return (
				error_printf(1, E_PCV0 " : postab[i %d] %d", i, sd->postab[i]));
		}
		i++;
	}
	return (0);
}

static ssize_t	find_next_number(t_sdat *sd, ssize_t curr_max, ssize_t *index)
{
	ssize_t	temp;
	ssize_t	local_max;
	ssize_t	i;

	i = 0;
	local_max = INT_MIN;
	while (i < sd->inp_siz)
	{
		temp = sd->postab[i].number;
		if (local_max <= temp && temp < curr_max)
		{
			local_max = temp;
			*index = i;
		}
		i++;
	}
	return (local_max);
}

int	populate_pos_table(t_sdat *sd)
{
	ssize_t		i;
	ssize_t		pos;
	ssize_t		next;

	if (fill_numbers_from_stack(sd))
	{
		return (error_put(1, E_PPT0));
	}
	i = 0;
	next = find_next_number(sd, ~(1 << 31) + 1L, &i);
	pos = sd->inp_siz - 1;
	while (i >= 0 && i < sd->inp_siz && pos >= 0)
	{
		sd->postab[i].pos = pos;
		pos--;
		next = find_next_number(sd, next, &i);
	}
	return (postab_check_valid(sd));
}
