/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_instrs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 09:44:42 by lorenuar          #+#    #+#             */
/*   Updated: 2021/05/22 23:12:54 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libcommon.h"

int	parse_instr(char *input)
{
	int			i;
	static char	*str_instrs[N_INSTR] = {
	"sa", "sb", "ss", "pa", "pb", "ra", "rb", "rr", "rra", "rrb", "rrr"};

	i = 0;
	while (i < N_INSTR)
	{
		if (!str_cmp(input, str_instrs[i]))
			return (i);
		i++;
	}
	return (error_printf(I_NVALID, E_PI0 " : \"%s\"", input));
}

int	read_instrs(int fd)
{
	int		instr;
	char	*input;
	int		ret;

	ret = get_next_line(fd, &input);
	if (ret < 0)
	{
		free(input);
		return (error_put(I_NVALID, E_RI0));
	}
	if (input && !input[0])
	{
		free(input);
		return (I_NVALID);
	}
	instr = parse_instr(input);
	free(input);
	return (instr);
}
