/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/15 17:03:51 by lorenuar          #+#    #+#             */
/*   Updated: 2021/05/17 21:17:26 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_H
# define CHECKER_H

# include "libutils.h"
# include "libcommon.h"

/*
** Errors
*/

# define E_C0 "checker : error from parse_args()"
# define E_C1 "checker : input is invalid"
# define E_SCL0 "sub_checker_loop : error from exec_instr()"
# define E_RSFA0 "read_stack_from_args : error from stack_push_data()"
# define E_RSFS0 "read_stack_from_str : error from stack_push_data()"

#endif
